using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class whosturn : MonoBehaviour
{

    public int whoTurn;//0 = o and 1 = x
    public int turnCount;//counts the number of turn played
    public Sprite[] playerIcons;//0 = o and 1 = x 
    public Button[] tictactoeSpaces;
    public int[] markedSpaces;//ID,s Which space mark by which player;
    public Text winnerText;//winnertext;
    public GameObject[] winningline;//winner lines for show
    public GameObject Resetbutt;
    void Awake()
    {
        GameSetup();
        Debug.Log("i am here");
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }
    void GameSetup()
    {
        whoTurn = 0;
        turnCount = 0;
        for (int i = 0; i < markedSpaces.Length; i++)
        {
            markedSpaces[i] = -100;
            tictactoeSpaces[i].image.sprite = playerIcons[2];
            tictactoeSpaces[i].interactable = true;
            if (i < 8)
            {
                winningline[i].SetActive(false);
            }
        }
        Resetbutt.SetActive(false);
        winnerText.text = "";
    }

    public void tictactoebutton(int whichNumber)
    {
        tictactoeSpaces[whichNumber].image.sprite = playerIcons[whoTurn];
        tictactoeSpaces[whichNumber].interactable = false;

        markedSpaces[whichNumber] = whoTurn+1;
        turnCount++;
        if (turnCount > 4)
        {
            WinnerCheck();
        }
        if (whoTurn == 0)
        {
            whoTurn = 1;
        }
        else
        {
            whoTurn = 0;
        }
    }
    void WinnerCheck()//check who is winner
    {
        int s1 = markedSpaces[0] + markedSpaces[1] + markedSpaces[2];
        int s2 = markedSpaces[3] + markedSpaces[4] + markedSpaces[5];
        int s3 = markedSpaces[6] + markedSpaces[7] + markedSpaces[8];
        int s4 = markedSpaces[0] + markedSpaces[3] + markedSpaces[6];
        int s5 = markedSpaces[1] + markedSpaces[4] + markedSpaces[7];
        int s6 = markedSpaces[2] + markedSpaces[5] + markedSpaces[8];
        int s7 = markedSpaces[0] + markedSpaces[4] + markedSpaces[8];
        int s8 = markedSpaces[2] + markedSpaces[4] + markedSpaces[6];
        var solutions = new int[] { s1, s2, s3, s4, s5, s6, s7, s8 };
        for (int i = 0; i < solutions.Length; i++)
        {
            if (solutions[i] == 3 * (whoTurn + 1))
            {
            Debug.Log("player" + whoTurn + "won");
            winnerdisplay(i);
            return;
            }
            else if(turnCount>=9)
            {
               winnerText.gameObject.SetActive(true);
               winnerText.text = "Draw!";
               Resetbutt.SetActive(true);
            }


        }
    }
    void winnerdisplay(int indexIn)
    {
        winnerText.gameObject.SetActive(true);
        if (whoTurn == 0)
        {
            winnerText.text = "Player O wins!";
            Resetbutt.SetActive(true);
        }
        else if (whoTurn == 1)
        {
            winnerText.text = "Player X wins!";
            Resetbutt.SetActive(true);
        }
        winningline[indexIn].SetActive(true);
        for (int i = 0; i < tictactoeSpaces.Length; i++)
        {
            tictactoeSpaces[i].interactable = false;
        }
    }
    public void restbutton()
    {
        GameSetup();
    }
}
