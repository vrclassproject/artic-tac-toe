using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;
using UnityEngine.XR.ARFoundation;
using UnityEngine.UI;

public class imageRecongnition : MonoBehaviour
{

    private ARTrackedImageManager arimage;
    public GameObject gamecanvas;

     void Awake()
    {
        arimage = FindObjectOfType<ARTrackedImageManager>();
        Debug.Log("working");
        
    }
     void OnEnable()
    {
        arimage.trackedImagesChanged += OnImageChanged;
    }
     void OnDisable()
    {
        arimage.trackedImagesChanged -= OnImageChanged;
    }
    public void OnImageChanged(ARTrackedImagesChangedEventArgs args)
    {
        foreach (var trackedImage in args.added)
        {
            Debug.Log("traked");
            gamecanvas.SetActive(true);

        }
        foreach (var trackedImage in args.updated)
        {
            Debug.Log("traked");
            gamecanvas.SetActive(true);
        }
        foreach (var trackedImage in args.removed)
        {
            Debug.Log("traked");
            gamecanvas.SetActive(false);
        }
    }
}
