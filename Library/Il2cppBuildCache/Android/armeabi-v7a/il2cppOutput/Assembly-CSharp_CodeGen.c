﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void imageRecongnition::Awake()
extern void imageRecongnition_Awake_m5373897FBD0FE4CFD4518AA8762B92F632E28DB5 (void);
// 0x00000002 System.Void imageRecongnition::OnEnable()
extern void imageRecongnition_OnEnable_m49226A4A3310C85E002C5990F7575E6105276160 (void);
// 0x00000003 System.Void imageRecongnition::OnDisable()
extern void imageRecongnition_OnDisable_m1A8A35AF8036F9CD0A519C6732FA68B375880DA7 (void);
// 0x00000004 System.Void imageRecongnition::OnImageChanged(UnityEngine.XR.ARFoundation.ARTrackedImagesChangedEventArgs)
extern void imageRecongnition_OnImageChanged_mFB68BA2F97DC7949338A83A8B143B925BB023213 (void);
// 0x00000005 System.Void imageRecongnition::.ctor()
extern void imageRecongnition__ctor_m3E7B62CAA6AB9CC356FF1DB91543F55F3D69FCF6 (void);
// 0x00000006 System.Void whosturn::Awake()
extern void whosturn_Awake_m1A2807FC82362F64B52E9DB42117F93A80DD32F7 (void);
// 0x00000007 System.Void whosturn::Start()
extern void whosturn_Start_m1E7F10B594B2C30B67E0E43F3D798B598E527D12 (void);
// 0x00000008 System.Void whosturn::GameSetup()
extern void whosturn_GameSetup_m1B684C91090C7B8EEB17B1CB76D709340AC957AF (void);
// 0x00000009 System.Void whosturn::tictactoebutton(System.Int32)
extern void whosturn_tictactoebutton_m03E15ED2E6657F025A0222212C35ABB9D3DF8D79 (void);
// 0x0000000A System.Void whosturn::WinnerCheck()
extern void whosturn_WinnerCheck_mB0D17DABD1B16116D20F21C89FBC65841BAD50DF (void);
// 0x0000000B System.Void whosturn::winnerdisplay(System.Int32)
extern void whosturn_winnerdisplay_m9730967D2D8B4D0E33EEC5C83168F4ADE286C519 (void);
// 0x0000000C System.Void whosturn::restbutton()
extern void whosturn_restbutton_mF7645B4AFDAD67F4C7F25184F5C48BB0EF68FD0F (void);
// 0x0000000D System.Void whosturn::.ctor()
extern void whosturn__ctor_m14CADD56E6AD0472ADBCC92E29EE181BC063566A (void);
static Il2CppMethodPointer s_methodPointers[13] = 
{
	imageRecongnition_Awake_m5373897FBD0FE4CFD4518AA8762B92F632E28DB5,
	imageRecongnition_OnEnable_m49226A4A3310C85E002C5990F7575E6105276160,
	imageRecongnition_OnDisable_m1A8A35AF8036F9CD0A519C6732FA68B375880DA7,
	imageRecongnition_OnImageChanged_mFB68BA2F97DC7949338A83A8B143B925BB023213,
	imageRecongnition__ctor_m3E7B62CAA6AB9CC356FF1DB91543F55F3D69FCF6,
	whosturn_Awake_m1A2807FC82362F64B52E9DB42117F93A80DD32F7,
	whosturn_Start_m1E7F10B594B2C30B67E0E43F3D798B598E527D12,
	whosturn_GameSetup_m1B684C91090C7B8EEB17B1CB76D709340AC957AF,
	whosturn_tictactoebutton_m03E15ED2E6657F025A0222212C35ABB9D3DF8D79,
	whosturn_WinnerCheck_mB0D17DABD1B16116D20F21C89FBC65841BAD50DF,
	whosturn_winnerdisplay_m9730967D2D8B4D0E33EEC5C83168F4ADE286C519,
	whosturn_restbutton_mF7645B4AFDAD67F4C7F25184F5C48BB0EF68FD0F,
	whosturn__ctor_m14CADD56E6AD0472ADBCC92E29EE181BC063566A,
};
static const int32_t s_InvokerIndices[13] = 
{
	2323,
	2323,
	2323,
	1839,
	2323,
	2323,
	2323,
	2323,
	1873,
	2323,
	1873,
	2323,
	2323,
};
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	13,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_AssemblyU2DCSharp_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
